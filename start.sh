#!/bin/bash

mkdir -p /app/data/var/log/prod /app/data/var/cache/prod /app/data/var/session

echo "==> Creating config"
if [[ ! -f /app/data/.initialized ]]; then
  echo "==> Initializing"
  cp /app/pkg/env.production /app/data/env

  touch /app/data/.initialized
fi

echo "==> Migrating DB"
php bin/console doctrine:migrations:migrate  --no-interaction

echo "==> Changing permissions"
chmod 777 -R /app/data/var
chown -R www-data:www-data /app/data/var

echo "==> Staring Koillection"

APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
